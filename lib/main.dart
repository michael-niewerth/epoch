//import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_nord_theme/flutter_nord_theme.dart';
import 'package:provider/provider.dart';
import 'dart:async';
import 'dart:io';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:date_format/date_format.dart';
// ignore_for_file: prefer_const_constructors
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  var databox = await Hive.openBox('epochdata');
  var box = await Hive.openBox('Tracks');
  //final tracks = {1:'Track 1',2:'Track2',3:'Track 3',4:'Track 4',5:'Track 5',6:'Track 6'};
  //box.putAll(tracks);
  //final Map<int,String> test = Map.from(box.toMap());
  print(Hive.boxExists('Tracks'));
  runApp(App());

}

class App extends StatelessWidget {
  const App({super.key});
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => AppState(),
      child: MaterialApp(
        title: 'timed App',
        themeMode: ThemeMode.dark,
        theme: ThemeData(
          appBarTheme: AppBarTheme(
            textTheme: const TextTheme(
              bodyText1: TextStyle(),
              bodyText2: TextStyle(),
            ).apply(
              bodyColor: Color(0xFFcdd6f4),
              //bodyColor: Colors.orange,
              //displayColor: Color(0xFFcdd6f4),
              displayColor: Color(0xFFcdd6f4),
            ),
          ),
          canvasColor: const Color(0xFF1e1e2e),
          textTheme: const TextTheme(
            bodyText1: TextStyle(),
            bodyText2: TextStyle(),
        ).apply(
            bodyColor: Color(0xFFcdd6f4),
            //bodyColor: Colors.orange,
            //displayColor: Color(0xFFcdd6f4),
            displayColor: Color(0xFFcdd6f4),
          ),
          buttonTheme:  const ButtonThemeData(
            buttonColor: Color(0xFF6c7086),
          ),
          elevatedButtonTheme:  ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
              primary: Color(0xFF45475a),
              onPrimary: Color(0xFFcdd6f4)
            )
          )
          //textTheme:
        ),
        //darkTheme: NordTheme.dark(),
        home: HomePage(),
      ),
    );
  }
}

class AppState extends ChangeNotifier{
  final box = Hive.box('Tracks');
  final databox = Hive.box('epochdata');
  //final tracks = {1:'Track 1',2:'Track2',3:'Track 3',4:'Track 4',5:'Track 5',6:'Track 6'};
  Map<int,String> trackmap = Map();
  var current = 'init';
  var csv = '';
  var activeId = 0;
  var addName = '';
  var addId = 0;
  var path = './';
  //var tbox = Hive.box();

  Map<int,String> getTracks(){
    box.keys.forEach((key) {trackmap[key as int] = box.get(key);});
    return trackmap;


  }
  void deleteID(int key){
    trackmap.remove(key);
    box.delete(key);
    notifyListeners();
  }
  void getPath() async {
    Directory dir = await getApplicationDocumentsDirectory();
    String pth = dir.path;
    path = pth;
    notifyListeners();
  }
  void getNext(){
    current = 'getnext';
    notifyListeners();
  }
  String getActiveName(){
    if(activeId != 0 && trackmap.containsKey(activeId) ){
      return trackmap[activeId]!+'  |  ID: $activeId';
    }else{
      return 'Paused';
    }
  }
  //void getFile(){
    //var file = File("/home/michael/z");
    //file.readAsString().then((String content) {
     // current = content;
   // });
    //notifyListeners();

  //}
  void setAddName(String inp){
    addName=inp;
    notifyListeners();
  }
  void setAddId(int inp){
    addId = inp;
    notifyListeners();
  }
  void getId(int id){
    var now = DateTime.now();
    //var time = now.day+now.month+now.year+now.hour+now.minute+now.second+now.millisecond;
    var time = formatDate(now, [dd,'-',mm,'-',yyyy,'_',HH,':',nn,':',ss]);
    if (activeId == id){
      appendFIle(",$time\n");
      csv += ",$time\n";
      activeId=0;
      databox.put('ID',0);

    }else if(activeId == 0){
      appendFIle(box.get(id)+",$time");
      csv+= box.get(id)+",$time";
      activeId=id;
      databox.put('ID',id);
    }else{
      appendFIle(",$time\n"+box.get(id)+",$time");
      csv+=",$time\n"+box.get(id)+",$time";
      activeId=id;
      databox.put('ID',id);

    }
    current = '$id,$time';
    notifyListeners();
  }
  void appendFIle(String content){
    var p = '${path}/epoch.csv';
    print(p);
    File(p).writeAsStringSync(content,mode:FileMode.append);
  }
  void addTask(BuildContext context){
    if(box.values.contains(addName)){
      showAlertDialog(context, 'Name is not unique');
    }else if(box.containsKey(addId)){
      showAlertDialog(context, 'ID is not unique');
    }else {
      box.put(addId, addName);
      trackmap[addId] = addName;
      notifyListeners();
    }
  }
  void bexists() async {
    var b = await Hive.boxExists('Tracks');
    print(b);
  }
}
showAlertDialog(BuildContext context, String errormsg) {

  // set up the button
  Widget okButton = TextButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context).pop(); // dismiss dialog
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Can't add Task"),
    content: Text(errormsg),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}



class HomePage extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<AppState>();
    if(appState.databox.containsKey('ID')) {
      appState.activeId = appState.databox.get('ID');
    }
    appState.getPath();
    return WillPopScope(
      onWillPop: () async => await _preExit(appState),
      child: Scaffold(
          appBar:PreferredSize(
            preferredSize: Size.fromHeight(80.0),
            child: Container(
                color: Color(0xF1181825),
                child: Wrap(
                    children: [Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.all(10),
                          margin:EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              border: Border.all(color: Color(0xFFcdd6f4))
                          ),
                          child: Text('Active Task:\n${appState.getActiveName()}'),
                        ),
                        const Spacer(),
                        const Padding(
                          padding: EdgeInsets.all(15),
                          child:Text('Epoch - Time Management',
                        style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),
                        ),),
                         const Spacer(),
                        Padding(
                          padding: const EdgeInsets.all(15),
                          child: ElevatedButton(onPressed: () {
                            appState.getId(appState.activeId);
                            SystemNavigator.pop();
                            }, child: Text('Exit')),
                        )

                      ],
                    ),            ]
                )
            ),
          ),
          body: FractionallySizedBox(
            widthFactor: 1,
            child: Column(

              //spacing: 20,
              crossAxisAlignment: CrossAxisAlignment.center,

                children:[
                  SizedBox(height: 30,),
                  Padding(
                    padding: EdgeInsets.all(30.0),
                    child:
                      Wrap(
                        spacing: 10,
                        runSpacing: 10,
                        //mainAxisAlignment: MainAxisAlignment.center,
                        children: [

                          //for(MapEntry<int,String> item in appState.tracks.entries)  ElevatedButton(onPressed: (){appState.getId(item.key);}, child: Padding(padding:EdgeInsets.all(50),child:Text(item.value))),
                          //for(String key in appState.box.keys.toList()) ElevatedButton(onPressed: (){appState.getId(key as int);}
                          for(MapEntry<int,String> item in appState.getTracks().entries)  ElevatedButton(onLongPress: (){appState.deleteID(item.key);},onPressed: (){appState.getId(item.key);}, child: Padding(padding:EdgeInsets.all(50),child:Text(item.value))),
                        ],
                      ),
                  ),
                  Container(
                    decoration: const BoxDecoration(
                        border: BorderDirectional(bottom: BorderSide(color: Color(0xFF9399b2),width: 1))
                    ),
                    child:  FractionallySizedBox(
                      widthFactor: 0.9,
                    ),
                  ),
                  Spacer(),
                 Container(
                        padding: EdgeInsets.all(10),
                        decoration: const BoxDecoration(
                            border: BorderDirectional(top: BorderSide(color: Color(0xFF9399b2),width: 1),start:BorderSide(color: Color(0xFF9399b2),width: 1),end: BorderSide(color: Color(0xFF9399b2),width: 1))
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text('Add new Task:',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),),
                              SizedBox(height: 5,),
                              //widthFactor: 0.7,
                              //heightFactor: 0.2,
                              SizedBox(
                                width: 200,
                                  height: 50,
                                  child:
                                  TextField(
                                    onChanged: (value) => appState.setAddName(value),

                                    style: TextStyle(color: Color(0xFFcdd6f4)),
                                    decoration: InputDecoration(
                                      labelText: ' Name',
                                      labelStyle: TextStyle(
                                        color: Color(0x64cdd6f4)
                                      ),
                                      hintStyle: TextStyle(
                                        color: Color(0x64bac2de),
                                      ),
                                      hintText: 'Enter your desired Name',
                                      enabledBorder: UnderlineInputBorder(
                                          borderSide:BorderSide(color: Color(0xFF9399b2))
                                      ),
                                    ),
                                  ),
                                ),
                            SizedBox(
                              width: 200,
                              height: 50,
                              child:
                              TextField(
                                onChanged: (value) => {if(value != ''){appState.setAddId(int.parse(value))}else{appState.setAddId(0)}},
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                style: TextStyle(color: Color(0x64cdd6f4)),
                                decoration: InputDecoration(
                                  labelText: 'ID',
                                  labelStyle: TextStyle(
                                      color: Color(0x64cdd6f4)
                                  ),
                                  hintStyle: TextStyle(
                                    color: Color(0x64bac2de),
                                  ),
                                  hintText: 'Enter your desired ID',
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide:BorderSide(color: Color(0xFF9399b2))
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 20),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: ElevatedButton(onPressed:() {appState.addTask(context);}, child: Text('Add'),),
                              ),
                            )

                          ],
                        ),
                      ),

                ]
            ),
          )
      ),
    );
  }
  Future<bool> _preExit(AppState appState) async{
    await {
      appState.getId(appState.activeId)
    };
    return true;
    return Future<bool>.value(true); // this will close the app,
    //return Future<bool>.value(false); // and this will prevent the app from exiting (by tapping the back button on home route)
  }
}
