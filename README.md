# epoch

A flutter App to track time spent on different Tasks.

## Installation
### Compile from source (All Platforms):
Clone the source code:<br><br>
```
git clone https://gitlab.com/michael-niewerth/epoch.git
cd epoch
```
<br><br>
Build for your platform in release mode:<br><br>
```
flutter build your_platform --release
``` 

the resulting Binary is gonna be in  `/build/your_platform/x64_release/bundle/` .

## Linux
Use the installer script provided with each release:

```
cd directory_containing_zip
unzip epoch_version_linux.zip
sudo ./install_on_linux.sh
```