#!/bin/bash

sudo mkdir /usr/lib/epoch
sudo cp -r * /usr/lib/epoch
sudo touch /usr/bin/epoch
sudo echo -e "#!/bin/bash\nexec /usr/lib/epoch/epoch &" >> /usr/bin/epoch
sudo chmod +x /usr/bin/epoch
